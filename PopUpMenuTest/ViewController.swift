//
//  ViewController.swift
//  PopUpMenuTest
//
//  Created by roman on 6/21/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGuestures()
        
    }

    private func setupGuestures() {
        
        let tapGuesture = UITapGestureRecognizer(target: self, action: #selector(tapped))
        tapGuesture.numberOfTapsRequired = 1
        button.addGestureRecognizer(tapGuesture)
    }
    
    @objc
    private func tapped() {
        
        guard let popVC = storyboard?.instantiateViewController(withIdentifier: "popVC") else { return }
        
        popVC.modalPresentationStyle = .popover
        
        let popOverVC = popVC.popoverPresentationController
        popOverVC?.delegate = self
        popOverVC?.sourceView = self.button
        popOverVC?.sourceRect = CGRect(x: self.button.bounds.midX, y: self.button.bounds.minY, width: 0, height: 0)
        
        popVC.preferredContentSize = CGSize(width: 250, height: 250)
        
        self.present(popVC, animated: true)
    }
    
}

extension ViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
}
